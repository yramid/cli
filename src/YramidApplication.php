<?php

declare(strict_types=1);

namespace Yramid;

use Composer\InstalledVersions;
use LogicException;
use Symfony\Component\Console\Application;

class YramidApplication extends Application
{
    private const CLI_PACKAGE = 'yramid/cli';
    private const CMD_PACKAGE = 'yramid/symfony-commands';
    private const LIB_PACKAGE = 'yramid/lib';

    private const COMMAND_MAP = [
        Migration\Migrate::class => ['migration:up', 'migrate'],
        Migration\Rollback::class => ['migration:down', 'rollback'],
        Migration\Create::class => ['migration:create', 'create'],
        Migration\Status::class => ['migration:status', 'status'],

        Seed\Create::class => ['seed:create'],
        Seed\Run::class => ['seed:run'],

        Migration\Savepoint\Set::class => ['savepoint:set'],
        Migration\Savepoint\Remove::class => ['savepoint:unset'],
    ];

    private string $configFile;
    private Yramid $yramid;

    public function __construct()
    {
        parent::__construct(
            'Yramid',
            self::getVersionString(),
        );

        $this->configFile = self::getConfigFile();
        $this->yramid = new Yramid();

        $this->registerCommands();
    }

    private static function getVersionString(): string
    {
        $cliVersion = self::getPackageVersion(self::CLI_PACKAGE);
        $cmdVersion = self::getPackageVersion(self::CMD_PACKAGE);
        $libVersion = self::getPackageVersion(self::LIB_PACKAGE);

        return "$cliVersion (yramid/lib $libVersion, yramid/symfony-commands $cmdVersion)";
    }

    private static function getPackageVersion(string $package): string
    {
        return InstalledVersions::getPrettyVersion($package) ?? 'unknown';
    }

    private function registerCommands(): void
    {
        $cmd = function (string $class, string ...$names) {
            $name = array_shift($names);
            $aliases = $names;

            $command = new $class($name, $this->configFile, $this->yramid);

            if ($aliases) {
                $command->setAliases($aliases);
            }

            return $command;
        };

        foreach (self::COMMAND_MAP as $class => $names) {
            $this->add($cmd($class, ...$names));
        }
    }

    private static function getConfigFile(): string
    {
        $configCandidates = [
            __DIR__ . '/../../../../.yramid.php', // as dependency
            __DIR__ . '/../.yramid.php' // standalone
        ];

        foreach ($configCandidates as $configCandidate) {
            if (is_file($configCandidate)) {
                return realpath($configCandidate);
            }
        }

        throw new LogicException('Missing config file');
    }
}
