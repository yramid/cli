<?php

declare(strict_types=1);

namespace Yramid;

$autoloadCandidates = [
    $_composer_autoload_path ?? '', // composer 2.2
    __DIR__ . '/../../../../vendor/autoload.php', // as dependency
    __DIR__ . '/../vendor/autoload.php' // standalone
];

foreach ($autoloadCandidates as $file) {
    if (!is_file($file)) {
        continue;
    }
    include $file;
}
