<?php

declare(strict_types=1);

namespace Yramid;

require_once __DIR__ . '/autoload.php';

return new YramidApplication();
